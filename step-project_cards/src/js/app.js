import * as flsFunctions from './modules/functions.js'
import LoginModal from './modules/modalSubClass/LoginModal.js'
import VisitModal from './modules/modalSubClass/VisitModal.js'
import * as filterVisits from './modules/modalSubClass/filterCards.js'

flsFunctions.isWebp()

//Функція для запуску початкової логіки на сторінці
function initialFunction() {
  const loginModal = new LoginModal('#modal-login', '#btn-open-modal-login')
  const visitModal = new VisitModal('#modal-visit', '#btn-open-modal-visit')
}
initialFunction()
