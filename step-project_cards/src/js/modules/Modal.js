export default class Modal {
  constructor(modalSelector, triggerBtnSelector) {
    this.modal = document.querySelector(modalSelector)
    this.openBtn = document.querySelector(triggerBtnSelector)
    this.closeBtn = this.modal.querySelector('.close-btn')
    this.overlay = this.modal.querySelector('.overlay')

    this.open = this.open.bind(this)
    this.close = this.close.bind(this)
    this.overlayClickHandler = this.overlayClickHandler.bind(this)
    this.closeBtnClickHandler = this.closeBtnClickHandler.bind(this)
    this.openBtnClickHandler = this.openBtnClickHandler.bind(this)

    this.addEventListeners()
  }

  addEventListeners() {
    this.overlay.addEventListener('click', this.overlayClickHandler)
    this.openBtn.addEventListener('click', this.openBtnClickHandler)
    this.closeBtn.addEventListener('click', this.closeBtnClickHandler)
  }

  open() {
    this.modal.classList.add('open')
  }

  close() {
    this.modal.classList.remove('open')
    console.log('close')
  }

  overlayClickHandler(e) {
    if (e.target === this.overlay) {
      this.close()
    }
  }

  closeBtnClickHandler() {
    this.close()
  }

  openBtnClickHandler() {
    this.open()
  }
}
