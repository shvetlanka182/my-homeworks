import Modal from '../Modal.js'
import { dataService } from '../services.js'
import { VisitView } from '../visitController.js'
import { noVisitsInfo } from '../main.js'
noVisitsInfo()
export default class LoginModal extends Modal {
  constructor(modalSelector, triggerBtnSelector) {
    super(modalSelector, triggerBtnSelector)
    this.loginForm = this.modal.querySelector('.login-form')
    this.loginFormSubmitHandler = this.loginFormSubmitHandler.bind(this)
    this.loginForm.addEventListener('submit', this.loginFormSubmitHandler)
  }

  // перевірка логіну та пароля
  //Your token is: fb2bd690-9ef2-456f-a39b-ea71440e1825
  //Authorization: `Bearer ${token}`

  async loginFormSubmitHandler(e) {
    e.preventDefault()
    const email = this.loginForm.querySelector('#email').value
    const password = this.loginForm.querySelector('#password').value

    try {
      const loginResponse = await fetch(
        'https://ajax.test-danit.com/api/v2/cards/login',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ email, password }),
        }
      )
      const userToken = await loginResponse.text()

      if (!loginResponse.ok) {
        throw new Error(userToken)
      }

      localStorage.setItem('userToken', userToken)
      console.log(userToken)

      const visitsView = new VisitView()
      visitsView.getAllVisits()

      //зміна кнопки на logout
      this.logout()

      this.close()
    } catch (error) {
      console.error(error)
    }
    // getToken.localStorage.getItem()
  }

  logout() {
    const logoutBtn = document.querySelector('#btn-logout')
    const loginBtn = document.querySelector('#btn-open-modal-login')

    loginBtn.classList.add('login-hide')
    logoutBtn.classList.remove('logout-hide')

    const logoutClick = function () {
      if (confirm('Are you sure you want to log out?')) {
        //для виходу - видаляю токен з локал стор
        localStorage.removeItem('userToken')
        logoutBtn.classList.add('logout-hide')
        loginBtn.classList.remove('login-hide')
        logoutBtn.removeEventListener('click', logoutClick)
        noVisitsInfo()
      }
    }

    logoutBtn.addEventListener('click', logoutClick)
  }
}
