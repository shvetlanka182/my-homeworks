// Filter.js
import { VisitView } from '../visitController.js'
import { dataService } from '../services.js'

const searchInput = document.getElementById('search')
const filterStatus = document.getElementById('filter_done')
const filterPriority = document.getElementById('filter_priority')
const searchButton = document.getElementById('search__btn')

searchInput.addEventListener('input', filterVisits)
filterStatus.addEventListener('change', filterVisits)
filterPriority.addEventListener('change', filterVisits)
searchButton.addEventListener('click', filterVisits)

async function filterVisits() {
  const searchText = searchInput.value.toLowerCase()
  const status = filterStatus.value
  const priority = filterPriority.value

  const allVisits = await dataService.allVisits
  let filteredVisits = allVisits.filter((visit) => {
    console.log(visit)
    const titleMatch =
      visit.name.toLowerCase().includes(searchText) ||
      visit.description.toLowerCase().includes(searchText) ||
      visit.doctor.toLowerCase().includes(searchText)
    const statusMatch =
      status === 'all' ||
      visit.completed?.toLowerCase() === status.toLowerCase()
    const priorityMatch =
      priority === 'all' ||
      visit.urgency?.toLowerCase() === priority.toLowerCase()

    return titleMatch && statusMatch && priorityMatch
  })
  console.log(filteredVisits)
  const visitView = new VisitView()
  visitView.insertAllVisits(visitView.changeObjectArray(filteredVisits))
}
