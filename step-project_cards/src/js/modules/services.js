function getToken() {
  return localStorage.getItem('userToken')
}

const URL_VISITS = `https://ajax.test-danit.com/api/v2/cards`

// обєкт з бази даних
export const dataService = {
  get allVisits() {
    return fetch(URL_VISITS, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((data) => data)
      .catch((error) => console.log(error))
  },

  getVisit(visitId) {
    return fetch(`${URL_VISITS}/${visitId}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .catch((error) => console.log(error))
  },

  createVisit(visit) {
    return fetch(URL_VISITS, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(visit),
    })
  },

  updateVisit(visitId, visit) {
    return fetch(`${URL_VISITS}/${visitId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(visit),
    }).catch((error) => console.log(error))
  },

  deleteVisit(visitId) {
    return fetch(`${URL_VISITS}/${visitId}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    }).catch((error) => console.log(error))
  },
}
