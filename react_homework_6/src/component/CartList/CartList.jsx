import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import CartItem from '../CartItem/CartItem'
import styles from './CartList.module.scss'

const CartList = () => {
  const cart = useSelector((state) => state.cart.cart)
  const sum = cart.reduce((acc, cur) => {
    return acc + cur.price * cur.quantity
  }, 0)
  return (
    <div className={styles.cart}>
      {!cart.length && 'Cart is empty'}
      {cart?.map((item, index) => {
        return <CartItem key={index} item={item} />
      })}
      {!!cart.length && (
        <div className={styles.order}>
          <div className={styles.order_sum}>{sum} грн.</div>
          <Link to="/order" className={styles.order_btn}>
            Оформити замовлення
          </Link>
        </div>
      )}
    </div>
  )
}

export default CartList
