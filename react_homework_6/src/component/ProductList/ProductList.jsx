import React, { useContext } from 'react'
import ProductCard from '../ProductCard/ProductCard'
import styles from './ProductList.module.scss'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux'
import { ToggleProductsListContext } from '../../contexts/contexts'
import { MdTableRows } from 'react-icons/md'
import { FaTableCellsLarge } from 'react-icons/fa6'

const ProductList = () => {
  const products = useSelector((state) => state.product.products)
  const { toggleView, setToggleView } = useContext(ToggleProductsListContext)
  const classView = toggleView ? styles.products_card : styles.products_table

  const handleToggleView = (e) => {
    setToggleView(!toggleView)
  }

  return (
    <div>
      <div className={styles.toggle}>
        {!toggleView ? (
          <FaTableCellsLarge
            className={styles.svg_icon}
            onClick={handleToggleView}
          />
        ) : (
          <MdTableRows className={styles.svg_icon} onClick={handleToggleView} />
        )}
      </div>
      <div className={classView}>
        {products.map((item) => {
          return <ProductCard item={item} key={item.article} />
        })}
      </div>
    </div>
  )
}

ProductList.propTypes = {
  favorite: PropTypes.array,
  toggleAcceptCartModal: PropTypes.func,
  toggleProductInCart: PropTypes.func,
}

export default ProductList
