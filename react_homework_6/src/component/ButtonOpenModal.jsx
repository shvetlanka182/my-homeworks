import React from 'react'
import styled from 'styled-components'

const Button = styled.button`
  color: #fff;
  border: none;
  padding: 10px 20px;
  font-size: 16px;
  border-radius: 5px;
  cursor: pointer;
  margin: 10px;

  &:hover {
    opacity: 0.7;
  }
`

const ButtonOpenModal = ({ text, backgroundColor, onClick }) => {
  return (
    <Button
      onClick={onClick}
      $backgroundColor={backgroundColor}
      style={{ backgroundColor: backgroundColor }}
    >
      {text}
    </Button>
  )
}

export default ButtonOpenModal
