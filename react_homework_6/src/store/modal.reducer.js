const TOGGLE_ACCEPT_ADD_MODAL = 'TOGGLE_ACCEPT_ADD_MODAL'
const TOGGLE_ACCEPT_DELETE_MODAL = 'TOGGLE_ACCEPT_DELETE_MODAL'

const initialState = {
  isAcceptDeleteModal: false,
  isAcceptAddModal: false,
}

const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_ACCEPT_ADD_MODAL:
      return {
        ...state,
        isAcceptAddModal: !state.isAcceptAddModal,
      }
    case TOGGLE_ACCEPT_DELETE_MODAL:
      return {
        ...state,
        isAcceptDeleteModal: !state.isAcceptDeleteModal,
      }

    default:
      return state
  }
}

export const toggleAcceptAddModal = () => ({
  type: TOGGLE_ACCEPT_ADD_MODAL,
})
export const toggleAcceptDeleteModal = () => ({
  type: TOGGLE_ACCEPT_DELETE_MODAL,
})

export default modalReducer
