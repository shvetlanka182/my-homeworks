import { applyMiddleware, createStore } from 'redux'
import { composeWithDevTools } from '@redux-devtools/extension'
import { combineReducers } from 'redux'
import thunk from 'redux-thunk'
import favoriteReducer from './favorite.reducer'
import productReducer from './product.reducer'
import cartReducer from './cart.reducer'
import modalReducer from './modal.reducer'

const rootReducer = combineReducers({
  favorite: favoriteReducer,
  product: productReducer,
  cart: cartReducer,
  modal: modalReducer,
})

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
)

export default store
