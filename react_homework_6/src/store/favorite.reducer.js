const SET_FAVORITE = 'SET_FAVORITE'

const initialState = {
  favorite: [],
}

const favoriteReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_FAVORITE:
      return {
        ...state,
        favorite: action.payload,
      }
    default:
      return state
  }
}

export const setFavorite = (data) => ({
  type: SET_FAVORITE,
  payload: data,
})
export const toggleProductInFavorite = (product) => {
  return (dispatch, getState) => {
    const currentFavorite = getState().favorite.favorite
    const index = currentFavorite.findIndex(
      (item) => item.article === product.article
    )
    if (index === -1) {
      const newFavorite = [...currentFavorite, product]
      dispatch(setFavorite(newFavorite))
    } else {
      const newFavorite = [...currentFavorite]
      newFavorite.splice(index, 1)
      dispatch(setFavorite(newFavorite))
    }
  }
}

export default favoriteReducer
