import axios from 'axios'

const SET_PRODUCTS = 'SET_PRODUCTS'
const SET_PRODUCT = 'SET_PRODUCT'

const initialState = {
  products: [],
  selectedProduct: null,
}

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PRODUCTS:
      return {
        ...state,
        products: [...action.payload],
      }
    case SET_PRODUCT:
      return {
        ...state,
        selectedProduct: action.payload,
      }
    default:
      return state
  }
}

export const setProducts = (data) => ({
  type: SET_PRODUCTS,
  payload: data,
})

export const setSelectedProduct = (product) => ({
  type: SET_PRODUCT,
  payload: product,
})

export const fetchProducts = () => {
  return async (dispatch) => {
    const response = await axios('/data.json')
    const products = await response.data

    dispatch(setProducts(products))
  }
}

export default productReducer
