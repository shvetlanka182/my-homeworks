import React from 'react'
import '@testing-library/jest-dom/'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import ButtonOpenModal from '../component/ButtonOpenModal'

describe('Button component', () => {
  const text = 'Click me'
  const backgroundColor = 'blue'
  const onClick = jest.fn()

  it('should render a button', () => {
    const view = render(<ButtonOpenModal text={text} />)
    expect(view).toMatchSnapshot()
  })
  it('should call the onClick function when clicked', async () => {
    render(<ButtonOpenModal text={text} onClick={onClick} />)
    const button = screen.getByText(text)
    await userEvent.click(button)
    expect(onClick).toHaveBeenCalled()
  })
  it('renders with backgroundColor prop', () => {
    render(<ButtonOpenModal text={text} backgroundColor={backgroundColor} />)
    const button = screen.getByText(text)
    expect(button).toHaveStyle({ backgroundColor })
  })
})
