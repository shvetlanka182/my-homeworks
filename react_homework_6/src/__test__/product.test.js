import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

const mockStore = configureStore([thunk])

import productReducer, {
  setProducts,
  setSelectedProduct,
  fetchProducts,
} from '../store/product.reducer'

describe('productReducer', () => {
  it('should return the initial state', () => {
    const initialState = {
      products: [],
      selectedProduct: null,
    }

    expect(productReducer(undefined, {})).toEqual(initialState)
  })

  it('should handle SET_PRODUCTS action', () => {
    const products = [
      { id: 1, name: 'Product 1' },
      { id: 2, name: 'Product 2' },
    ]
    const action = setProducts(products)

    const initialState = {
      products: [],
      selectedProduct: null,
    }

    const expectedState = {
      products,
      selectedProduct: null,
    }

    expect(productReducer(initialState, action)).toEqual(expectedState)
  })

  it('should handle SET_PRODUCT action', () => {
    const product = { id: 1, name: 'Selected Product' }
    const action = setSelectedProduct(product)

    const initialState = {
      products: [],
      selectedProduct: null,
    }

    const expectedState = {
      products: [],
      selectedProduct: product,
    }

    expect(productReducer(initialState, action)).toEqual(expectedState)
  })

  it('fetch products ', () => {
    const mock = new MockAdapter(axios)
    const store = mockStore({})
    const mockResponse = [
      { id: 1, name: 'Product 1' },
      { id: 2, name: 'Product 2' },
    ]

    mock.onGet('/data.json').reply(200, mockResponse)

    const expectedActions = [setProducts(mockResponse)]

    return store.dispatch(fetchProducts()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
