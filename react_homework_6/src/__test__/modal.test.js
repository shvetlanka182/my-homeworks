import React from 'react'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import Modal from '../component/Modal'

describe('Modal component', () => {
  const header = 'Header'
  const text = 'Text'
  const closeModal = jest.fn()

  it('renders modal with header and text', () => {
    render(
      <Modal header={header} text={text} closeModal={closeModal} actions={[]} />
    )

    const headerElement = screen.getByText(header)
    const textElement = screen.getByText(text)

    expect(headerElement).toBeInTheDocument()
    expect(textElement).toBeInTheDocument()
  })

  it('when click outside the modal', async () => {
    render(
      <Modal header={header} text={text} closeModal={closeModal} actions={[]} />
    )

    const shadow = screen.getByTestId('modal-shadow')
    await userEvent.click(shadow)

    expect(closeModal).toHaveBeenCalled()
  })

  it('calls the changeCart and closeModal functions when clicking the accept button', async () => {
    const changeCart = jest.fn()
    const acceptButton = 'Accept'

    render(
      <Modal
        header={header}
        text={text}
        closeModal={closeModal}
        changeCart={changeCart}
        actions={[acceptButton]}
      />
    )

    const acceptButtonElement = screen.getByText(acceptButton)
    await userEvent.click(acceptButtonElement)

    expect(changeCart).toHaveBeenCalled()
    expect(closeModal).toHaveBeenCalled()
  })
})
