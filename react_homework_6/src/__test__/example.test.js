import React from 'react'
import { render } from '@testing-library/react'
import configureStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import { MemoryRouter } from 'react-router-dom'
import thunk from 'redux-thunk'
import App from '../App'

const mockStore = configureStore([thunk])

describe('<App />', () => {
  it('should work', () => {
    const store = mockStore({
      cart: { cart: [] },
      favorite: { favorite: [] },
      modal: { isAcceptDeleteModal: false, isAcceptAddModal: false },
      product: { products: [], selectedProduct: null },
    })
    render(
      <Provider store={store}>
        <MemoryRouter>
          <App />
        </MemoryRouter>
      </Provider>
    )
    expect(1 + 1).toBe(2)
  })
})
