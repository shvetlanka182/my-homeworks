import React from 'react'
import { useState } from 'react'
import { ToggleProductsListContext } from './contexts'

export const ToggleProductsListProvider = ({ children }) => {
  const [toggleView, setToggleView] = useState(true)

  const value = {
    toggleView,
    setToggleView,
  }

  return (
    <ToggleProductsListContext.Provider value={value}>
      {children}
    </ToggleProductsListContext.Provider>
  )
}
