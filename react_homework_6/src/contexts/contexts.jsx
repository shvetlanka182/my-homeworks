import { createContext } from 'react'

export const ToggleProductsListContext = createContext(null)
