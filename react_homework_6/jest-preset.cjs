module.exports = {
  verbose: true,
  preset: 'react',
  testEnvironment: 'jsdom',
  extensionsToTreatAsEsm: ['.jsx'],
  moduleFileExtensions: ['js', 'jsx', 'json', 'node'],
  setupFilesAfterEnv: ['./src/setupTests.js'],
  roots: ['<rootDir>/src'],
  moduleNameMapper: {
    '\\.(css|less|scss)$': 'identity-obj-proxy',
    '\\.(gif|ttf|eot|svg|png)$': '<rootDir>/test/__ mocks __/fileMock.js',
  },
  transform: {
    '^.+\\.(js|jsx)$': 'babel-jest',
  },

  // babelConfig: {
  //   presets: [
  //     [
  //       '@babel/preset-env',
  //       {
  //         targets: {
  //           node: 'current',
  //         },
  //       },
  //     ],
  //     '@babel/preset-react',
  //   ],
  //   plugins: ['@babel/plugin-syntax-jsx'],
  // },
  transformIgnorePatterns: ['node_modules/(?!(variables)/)'],
}

// module.exports = {
//   verbose: true,
//   preset: 'react',
//   testEnvironment: 'jsdom',
//   extensionsToTreatAsEsm: ['.jsx'],
//   moduleFileExtensions: ['js', 'jsx', 'json', 'node'],
//   setupFilesAfterEnv: ['./src/setupTests.js'],
//   roots: ['<rootDir>/src'],
//   transform: {
//     '^.+\\.(js|jsx)$': [
//       'babel-jest',
//       {
//         configFile: './babel.config.js',
//       },
//     ],
//   },
//   cache: true,
//   testMatch: ['**/__tests__/**/*.js?(x)', '**/?(*.)+(spec|test).js?(x)'],
//   moduleNameMapper: {
//     '\\.(css|less|scss)$': 'identity-obj-proxy',
//     '\\.(gif|ttf|eot|svg|png)$': '<rootDir>/test/__ mocks __/fileMock.js',
//   },
//   testPathIgnorePatterns: ['<rootDir>/node_modules/'],
//   transformIgnorePatterns: [
//     '<rootDir>/node_modules/(?!(variables|other-package)/)',
//   ],
// }
