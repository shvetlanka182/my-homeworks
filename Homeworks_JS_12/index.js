//1. Події клавіатури можна використовувати для відстеження введення даних у полях форми, але 
// цей спосіб має обмеження. За його допомогою неможливо відстежити введення даних, 
// якщо не використовується клавіатура, наприклад, за допомогою миші та контекстного меню або розпізнавання мови. 
// Тому найкращим вибором для відстеження змін у полях форм будуть події, пов'язані 
// зі зміною полів форми – change, input.


const buttons = document.querySelectorAll('.btn');
document.addEventListener('keydown', handler);

function handler(element) {
    for (let i = 0; i < buttons.length; i++) {
        if ('Key' + buttons[i].textContent === element.code || buttons[i].textContent === element.key) {
            buttons[i].style.backgroundColor = 'blue';
        } else (buttons[i].style.backgroundColor = 'black')
    }
}
