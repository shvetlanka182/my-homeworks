// ajax дозволяє надсилати запити на сервер і отримувати дані, не перезавантажуючи
// всю сторінку, та дозволяє взаємодіяти з сервером асинхронно.

async function sendRequest(url) {
  const response = await fetch(url)
  if (!response.ok) {
    throw new Error('Network response was not ok')
  }
  return response.json()
}

async function renderCharacters(film, charactersContainer) {
  for (const characterLink of film.characters) {
    try {
      const character = await sendRequest(characterLink)
      const li = document.createElement('li')
      li.innerText = character.name
      charactersContainer.appendChild(li)
    } catch (error) {
      console.error('Error fetching character:', error)
      const li = document.createElement('li')
      li.innerText = 'Unknown Character'
      charactersContainer.appendChild(li)
    }
  }
}

async function renderFilms(films) {
  const filmsList = document.getElementById('filmsList')
  for (const film of films) {
    const { episodeId, name, openingCrawl } = film

    const filmItem = document.createElement('li')
    const filmTitle = document.createElement('strong')
    filmTitle.innerText = `Episode ${episodeId}: ${name}`
    filmItem.appendChild(filmTitle)

    const openingCrawlLi = document.createElement('li')
    openingCrawlLi.innerText = `Opening Crawl: ${openingCrawl}`
    filmItem.appendChild(openingCrawlLi)

    const charactersHeader = document.createElement('li')
    charactersHeader.innerText = 'Characters:'
    filmItem.appendChild(charactersHeader)

    const charactersList = document.createElement('ul')
    filmItem.appendChild(charactersList)

    await renderCharacters(film, charactersList)

    filmItem.style.border = '1px solid #ccc'
    filmItem.style.padding = '10px'
    filmItem.style.marginBottom = '20px'
    filmItem.style.listStyle = 'none'
    filmTitle.style.fontSize = '18px'
    filmTitle.style.fontWeight = 'bold'
    filmTitle.style.margin = '0'
    charactersList.style.marginTop = '10px'
    charactersList.style.paddingLeft = '20px'

    filmsList.appendChild(filmItem)
  }
}

async function init() {
  try {
    const films = await sendRequest(
      'https://ajax.test-danit.com/api/swapi/films'
    )
    const filmsData = films.map(
      ({ episodeId, name, openingCrawl, characters }) => ({
        episodeId,
        name,
        openingCrawl,
        characters,
      })
    )
    await renderFilms(filmsData)
  } catch (error) {
    console.error('Error fetching films:', error)
  }
}

init()
