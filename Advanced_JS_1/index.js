//1. Прототипне наслідування в JavaScript дозволяє об'єктам
// спадковувати властивості та методи від інших об'єктів.
// Кожен об'єкт має внутрішню властивість [[Prototype]], що вказує
// на об'єкт-прототип, з якого можна успадкувати властивості.
// Пошук властивостей відбувається вздовж прототипного ланцюжка.

// 2.  super() у конструкторі класу-нащадка допомагає забезпечити
// належну ініціалізацію спадкованих властивостей та методів,
// а також надає доступ до функціональності батьківського класу,
// що дозволяє нам гнучко розширювати та налаштовувати класи-нащадки.

class Employee {
  constructor(name, age, salary) {
    this._name = name
    this._age = age
    this._salary = salary
  }

  get name() {
    return this._name
  }

  set name(value) {
    this._name = value
  }

  get age() {
    return this._age
  }

  set age(value) {
    this._age = value
  }

  get salary() {
    return this._salary
  }

  set salary(value) {
    this._salary = value
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary)
    this.lang = lang
  }

  get salary() {
    return super.salary * 3
  }
}

const programmer1 = new Programmer('John', 28, 5000, 'English')
const programmer2 = new Programmer('Svitlana', 31, 6000, 'Ukrainian')

console.log(programmer1)
console.log(programmer2)
