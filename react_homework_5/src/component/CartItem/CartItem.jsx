import React from 'react'
import { useDispatch } from 'react-redux'
import { setSelectedProduct } from '../../store/product.reducer'
import { toggleAcceptDeleteModal } from '../../store/modal.reducer'
import { changeQuantity } from '../../store/cart.reducer'
import styles from './CartItem.module.scss'

const CartItem = ({ item }) => {
  const dispatch = useDispatch()
  const handleDeleteFromCart = () => {
    dispatch(toggleAcceptDeleteModal())
    dispatch(setSelectedProduct(item))
  }
  const handleIncrementQuantity = () => {
    dispatch(changeQuantity(item.article, item.quantity + 1))
  }

  const handleDecrementQuantity = () => {
    if (item.quantity > 1) {
      dispatch(changeQuantity(item.article, item.quantity - 1))
    }
  }

  return (
    <div className={styles.item}>
      <div className={styles.item_image}>
        <img src={item.image} alt={item.name} />
      </div>
      <div className={styles.item_name}>{item.name}</div>
      <div className={styles.item_count}>
        <button
          className={styles.item_count_btn}
          onClick={handleIncrementQuantity}
        >
          +
        </button>
        <span>{item.quantity}</span>
        <button
          className={styles.item_count_btn}
          onClick={handleDecrementQuantity}
        >
          -
        </button>
      </div>
      <div className={styles.item_price}>{item.price * item.quantity} грн.</div>
      <button className={styles.item_delete} onClick={handleDeleteFromCart}>
        x
      </button>
    </div>
  )
}
export default CartItem
