import { Formik, Form, Field, ErrorMessage } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import * as Yup from 'yup'
import { clearCart } from '../../store/cart.reducer'
import styles from './Order.module.scss'

const Order = () => {
  const dispatch = useDispatch()
  const cart = useSelector((state) => state.cart)
  const initialValues = {
    firstName: '',
    lastName: '',
    phone: '',
    age: '',
    address: '',
  }
  const orderValidation = () => {
    return Yup.object().shape({
      firstName: Yup.string()
        .required(`Введіть ім'я`)
        .min(2, 'Мінімальна довжина імені - 2 символи')
        .max(50, 'Максимальна довжина імені - 50 символів'),
      lastName: Yup.string()
        .min(2, 'Мінімальна довжина прізвища - 2 символи')
        .max(50, 'Максимальна довжина прізвища - 50 символів')
        .required('Введіть прізвище'),
      age: Yup.number()
        .required('Введіть вік')
        .min(16, 'Мінімальний вік - 16 років')
        .max(100, 'Максимальний вік - 100 років'),
      address: Yup.string().required('Введіть адресу'),
      phone: Yup.number().required('Введіть номер телефону'),
    })
  }

  const handleSubmit = (values, { setSubmitting, resetForm }) => {
    const order = {
      products: cart,
      userData: values,
    }
    resetForm(initialValues)
    console.log(order)

    localStorage.removeItem('cart')
    dispatch(clearCart())

    setSubmitting(false)
  }

  return (
    <div>
      <h2>Оформлення замовлення</h2>
      <Formik
        initialValues={initialValues}
        validationSchema={orderValidation}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting }) => (
          <Form className={styles.order}>
            <label htmlFor="firstName" className={styles.order__label}>
              Ім'я
            </label>
            <Field
              name="firstName"
              type="text"
              className={styles.order__input}
            />
            <span className={styles.order__err}>
              <ErrorMessage name="firstName" />
            </span>

            <label htmlFor="lastName" className={styles.order__label}>
              Прізвище
            </label>
            <Field
              name="lastName"
              type="text"
              className={styles.order__input}
            />
            <span className={styles.order__err}>
              <ErrorMessage name="lastName" />
            </span>

            <label htmlFor="age" className={styles.order__label}>
              Вік
            </label>
            <Field name="age" type="number" className={styles.order__input} />
            <span className={styles.order__err}>
              <ErrorMessage name="age" />
            </span>

            <label htmlFor="address" className={styles.order__label}>
              Адреса
            </label>
            <Field name="address" type="text" className={styles.order__input} />
            <span className={styles.order__err}>
              <ErrorMessage name="address" />
            </span>

            <label htmlFor="phone" className={styles.order__label}>
              Телефон
            </label>
            <Field name="phone" type="number" className={styles.order__input} />
            <span className={styles.order__err}>
              <ErrorMessage name="phone" />
            </span>

            <button
              type="submit"
              disabled={isSubmitting}
              className={styles.order__btn}
            >
              Замовлення підтверджую
            </button>
          </Form>
        )}
      </Formik>
    </div>
  )
}

export default Order
