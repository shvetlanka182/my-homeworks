import React, { useEffect } from 'react'
import './App.scss'
import ProductList from './component/ProductList/ProductList'
import Menu from './component/Menu/Menu'
import Modal from './component/Modal'
import { Route, Routes } from 'react-router-dom'
import CartList from './component/CartList/CartList'
import FavoriteList from './component/FavoriteList/FavoriteList'
import Order from './component/Order/Order'
import { useDispatch, useSelector } from 'react-redux'
import { fetchProducts } from './store/product.reducer'
import { setCart, handleAddToCart, deleteFromCart } from './store/cart.reducer'
import { setFavorite } from './store/favorite.reducer'
import {
  toggleAcceptDeleteModal,
  toggleAcceptAddModal,
} from './store/modal.reducer'

function App() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchProducts())
  }, [])

  useEffect(() => {
    const cartFromLocalStorage = JSON.parse(localStorage.getItem('cart'))
    if (cartFromLocalStorage) dispatch(setCart(cartFromLocalStorage))
  }, [])
  useEffect(() => {
    const favoriteFromLocalStorage = JSON.parse(
      localStorage.getItem('favorite')
    )
    if (favoriteFromLocalStorage)
      dispatch(setFavorite(favoriteFromLocalStorage))
  }, [])

  const cart = useSelector((state) => state.cart.cart)
  const favorite = useSelector((state) => state.favorite.favorite)
  const selectedProduct = useSelector((state) => state.product.selectedProduct)

  const isAcceptDeleteModal = useSelector(
    (state) => state.modal.isAcceptDeleteModal
  )
  const isAcceptAddModal = useSelector((state) => state.modal.isAcceptAddModal)

  const header = 'Do you want to delete this file?'
  const closeButton = true
  const text = `Once you delete this file, it won’t be possible to undo this action.
  Are you sure you want to delete it?`
  const actions = [
    <React.Fragment key="Ok">Ok</React.Fragment>,
    <React.Fragment key="Cancel">Cancel</React.Fragment>,
  ]

  useEffect(() => {
    localStorage.setItem('favorite', JSON.stringify(favorite))
  }, [favorite])

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart))
  }, [cart])

  const handleAcceptAddModal = () => dispatch(toggleAcceptAddModal())
  const handleAcceptDeleteModal = () => dispatch(toggleAcceptDeleteModal())

  return (
    <div className="App">
      <div className="container">
        <Menu cart={cart} favorite={favorite} />
      </div>

      <div className="container">
        <Routes>
          <Route path="/" element={<ProductList />} />
          <Route path="/favorite" element={<FavoriteList />} />
          <Route path="/cart" element={<CartList />} />
          <Route path="/order" element={<Order />} />
        </Routes>
      </div>

      {isAcceptAddModal && (
        <Modal
          header={'Are sure you want to add the product to the cart?'}
          closeButton={true}
          actions={actions}
          closeModal={handleAcceptAddModal}
          changeCart={() => dispatch(handleAddToCart(selectedProduct))}
        />
      )}
      {isAcceptDeleteModal && (
        <Modal
          header={header}
          text={text}
          closeButton={closeButton}
          actions={actions}
          closeModal={handleAcceptDeleteModal}
          changeCart={() => dispatch(deleteFromCart(selectedProduct.article))}
        />
      )}
    </div>
  )
}

export default App
