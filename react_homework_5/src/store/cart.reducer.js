const SET_CART = 'SET_CART'
const CLEAR_CART = 'CLEAR_CART'
const ADD_TO_CART = 'ADD_TO_CART'
const DELETE_FROM_CART = 'DELETE_FROM_CART'
const CHANGE_QUANTITY = 'CHANGE_QUANTITY'

const initialState = {
  cart: [],
}

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CART:
      return {
        ...state,
        cart: action.payload,
      }
    case CLEAR_CART:
      return {
        ...state,
        cart: [],
      }
    case ADD_TO_CART:
      return {
        ...state,
        cart: [...state.cart, action.payload],
      }
    case DELETE_FROM_CART:
      return {
        ...state,
        cart: state.cart.filter(
          (product) => product.article !== action.payload
        ),
      }
    case CHANGE_QUANTITY:
      return {
        ...state,
        cart: state.cart.map((item) =>
          item.article === action.payload.article
            ? { ...item, quantity: action.payload.quantity }
            : item
        ),
      }
    default:
      return state
  }
}

export const setCart = (data) => ({
  type: SET_CART,
  payload: data,
})
export const clearCart = () => ({
  type: CLEAR_CART,
})
export const addToCart = (selectedProduct) => ({
  type: ADD_TO_CART,
  payload: selectedProduct,
})
export const deleteFromCart = (id) => ({
  type: DELETE_FROM_CART,
  payload: id,
})
export const changeQuantity = (article, quantity) => ({
  type: CHANGE_QUANTITY,
  payload: { article, quantity },
})
export const handleAddToCart = (product) => {
  return (dispatch, setState) => {
    const updatedCart = setState().cart.cart
    const itemIndex = updatedCart.findIndex(
      (item) => item.article === product.article
    )
    if (itemIndex !== -1) {
      updatedCart[itemIndex].quantity += 1
      dispatch(setCart(updatedCart))
    } else {
      dispatch(addToCart(product))
    }
  }
}

export default cartReducer
