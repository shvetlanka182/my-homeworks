// 1. Метод forEach - використовується для перебору масиву, дає можливість замінити цикл for.
// 2. Щоб очитити масив, треба встановити довжину на нуль.
// 3. За допомогою метода isArray() ми можемо дізнатись, що та чи інша змінна є масивом.

const test = ['hello', 'world', 23, '23', null, [1, 2, 3], {name: 'Petya'},''];
function filterBy(arr, type){
    return arr.filter(value => typeof value !== type);
}
console.log(filterBy(test, 'string'))


