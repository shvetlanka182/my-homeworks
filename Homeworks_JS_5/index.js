// 1. Методом об'єкта назівають функцію, яка є частиною об'єкта.
// 2. Значення властивості об'єкта можуть мати будь-який тип даних.
// 3. Поняття посилальний тип даних означає, що коли ми надаємо
// змінній значення у вигляді об'єкта, відбувається таке:
// JavaScript створює в оперативній пам'яті коп'ютера ділянку пам'яті 
// під новостворений об'єкт. У змінну зберігається посилання на цю ділянку пам'яті.


function createNewUser() {
    const firstName = prompt('Enter your first name:');
    const lastName = prompt('Enter your last name:');
    return {
        _firstName: firstName,
        _lastName: lastName,
        getLogin(){
        return this._firstName[0].toLowerCase() + this._lastName.toLowerCase()
        },
        setFirstName(name){
            this._firstName = name
        },
        getFirstName(){
            return this._firstName
        },
        setLastName(lname){
            this._lastName = lname
        },
        getLastName(){
            return this._lastName
        },
    }
}

const newUser = createNewUser();

console.log(newUser.getLogin())






