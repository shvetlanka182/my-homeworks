// 1. Новий HTML тег можна створити за допомогою document.createElement(tag);
// 2. Перший параметр це кодове слово, яке вказує куди вставляти відносно елемента.
//  Його значенння має бути:"beforebegin" – вставити html безпосередньо перед елементом, 
// "afterbegin" – вставити html в elem, на початку, "beforeend" – вставити html в elem, в кінці,
// "afterend" – вставити html безпосередньо після елемента.
// 3. Щоб видалити елемент зі сторінки, використовується метод node.remove().

const cities = ["Chernihiv", ["Chernivtsi", "Kiev","Borispol", "Irpin"], "Kharkiv", "Odessa", "Lviv"];

function getCities(arr, parent = document.body) {
    if (arr.length === 0) {
      return
    };
    const ul = document.createElement('ul');
    arr.forEach((el) => {
      if (Array.isArray(el)) {
        return getCities(el, ul)
      };
      const listItem = document.createElement('li');
      listItem.append(el);
      ul.append(listItem)
    });
    parent.append(ul);
  }
  
  getCities(cities);

  let time = 3;
  const div = document.createElement('div');
  document.body.append(div);
  let timer = setInterval(() =>{
    if(time === 0){
        document.body.innerHTML = '';
        clearInterval(timer);
    }
    div.innerText = time;
    time--;
    }, 1000);


    
