const darkThemeStorage = localStorage.getItem('key');
if(darkThemeStorage){
    document.querySelector('input').checked = true;
    document.body.classList.add('dark');
}

const changeTheme = () => {    
    const darkTheme = document.body.classList.contains('dark');
        if (darkTheme) {
        localStorage.removeItem("key");
        document.body.classList.remove('dark');
      } else {
        localStorage.setItem('key', 'dark');
        document.body.classList.add('dark');
      }
      
}
const toggleSwitch = document.querySelector('.toggle-switch');
toggleSwitch.addEventListener('mouseup', changeTheme);
