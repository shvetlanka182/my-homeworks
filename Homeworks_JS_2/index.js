// 1. Існує шість типів даних: Undefined, Null,
// Boolean, String, Number та Object.
// 2. Різниця між == та === полягає в тому, що при ==
// відбувається перетворення типів до спільного, а потім
// відбувається порівняння. Що до === то порівняння відбувається 
// одразу і якщо типи різні то ми отримаємо false.
// 3. Оператор - це інструкція того, яку саме операцію треба
// виконати.


let userName;

do {
userName = prompt('What is your name?', userName);
} while (userName === "" || userName === null || !isNaN(userName));

let userAge = 0;

do {
userAge = +prompt('What is your age?', userAge);
} while (userAge === 0 || isNaN(userAge));

if (userAge < 18) {
    alert('You are not allowed to visit this website')
} else if (userAge >= 18 && userAge <= 22) {
    let userChoice = confirm('Are you sure you want to continue?')
    if (userChoice) {
        alert('Welcome ' + userName)
    } else {
        alert('You are not allowed to visit this website') 
    }
} else {
    alert('Welcome ' + userName)
}
    