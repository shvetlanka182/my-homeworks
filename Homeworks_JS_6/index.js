// 1. Щоб використовувати спеціальний символ як звичайний, треба додати до нього обернену косу рису: \.
// Це називається "екранування символу". Якщо ми шукаємо зворотну косу межу \ ,
// це спеціальний символ як у звичайних рядках, так і в регулярних виразах,
// тому ми маємо подвоїти її.
// 2. Спочатку ми пишемо function. Потім — назву функції і список її параметрів в дужках.
// Якщо параметрів немає, ми залишаємо пусті дужки. Код функції, який також називають тілом функції між фігурними дужками.
// 3. hoisting — це механізм JavaScript, в якому змінні та оголошення функцій, пересуваються вгору своєї області видимості
// перед тим, як код буде виконано. Як наслідок, це означає те, що зовсім неважливо де були оголошені функція або змінні,
// всі вони пересуваються вгору своєї області видимості, незалежно від того, локальна вона чи глобальна.

function changeFormatDate(date) {
  const year = date.slice(6, 10);
  const month = date.slice(3, 5);
  const day = date.slice(0, 2);

  return year + "-" + month + "-" + day;
}

function createNewUser() {
  const firstName = prompt("Enter your first name:");
  const lastName = prompt("Enter your last name:");
  const birthday = prompt("Enter your birthday: dd.mm.yyyy");
  return {
    _firstName: firstName,
    _lastName: lastName,
    birthday,
    getLogin() {
      return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
    },
    getAge() {
      const birthday = new Date(changeFormatDate(this.birthday));
      const dateNow = new Date();
      let age = dateNow.getFullYear() - birthday.getFullYear();
      if ((dateNow.getMonth() - birthday.getMonth() < 0) 
        || ((dateNow.getMonth() - birthday.getMonth() === 0) 
        && (dateNow.getDate() - birthday.getDate() < 0))){
        age--;    
      }
      return age;
    },
    getPassword() {
      return (
        this._firstName[0].toUpperCase() +
        this._lastName.toLowerCase() +
        this.birthday.slice(6, 10)
      );
    },  
      
    setFirstName(name) {
      this._firstName = name;
    },
    getFirstName() {
      return this._firstName;
    },
    setLastName(lname) {
      this._lastName = lname;
    },
    getLastName() {
      return this._lastName;
    },
  };
}

const newUser = createNewUser();
console.log(newUser);

console.log(newUser.getPassword());

console.log(newUser.getAge());
