import React, { useState } from 'react'
import './App.css'
import ButtonOpenModal from './component/ButtonOpenModal'
import Modal from './component/Modal'

function App() {
  const [isActiveFirstModal, setIsActiveFirstModal] = useState(false)
  const [isActiveSecondModal, setIsActiveSecondModal] = useState(false)

  const header = 'Do you want to delete this file?'
  const closeButton = true
  const text = `Once you delete this file, it won’t be possible to undo this action.
  Are you sure you want to delete it?`
  const actions = [
    <React.Fragment key="Ok">Ok</React.Fragment>,
    <React.Fragment key="Cancel">Cancel</React.Fragment>,
  ]
  const actions1 = [
    <React.Fragment key="Light Side">May the Force be with you</React.Fragment>,
    <React.Fragment key="Dark Side">
      May the Dark Side be with you
    </React.Fragment>,
  ]

  const toggleFirstModal = () => setIsActiveFirstModal(!isActiveFirstModal)

  const toggleSecondModal = () => setIsActiveSecondModal(!isActiveSecondModal)

  return (
    <div className="App">
      {isActiveFirstModal && (
        <Modal
          header={header}
          closeButton={closeButton}
          text={text}
          actions={actions}
          closeModal={toggleFirstModal}
        />
      )}
      {isActiveSecondModal && (
        <Modal
          header={'Do you want to be a Jedi?'}
          closeButton={false}
          text={
            'Jedi are skilled and disciplined warriors trained in the ways of the Force, striving for peace and justice in the galaxy.'
          }
          actions={actions1}
          closeModal={toggleSecondModal}
        />
      )}
      <ButtonOpenModal
        text="Open First Modal"
        backgroundColor="blue"
        onClick={toggleFirstModal}
      />
      <ButtonOpenModal
        text="Open Second Modal"
        backgroundColor="green"
        onClick={toggleSecondModal}
      />
    </div>
  )
}

export default App
