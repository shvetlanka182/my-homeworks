// Конструкція try...catch використовується в коді для обробки винятків,
// якщо виконуються операції, які можуть викликати помилки.
// Також її варто використовувати в ситуаціях, коли хочемо
// запобігти виходу програми з-під контролю при виникненні помилки,
// і замість цього забезпечити можливість обробити помилку і продовжити виконання програми.

const books = [
  {
    author: 'Люсі Фолі',
    name: 'Список запрошених',
    price: 70,
  },
  {
    author: 'Сюзанна Кларк',
    name: 'Джонатан Стрейндж і м-р Норрелл',
  },
  {
    name: 'Дизайн. Книга для недизайнерів.',
    price: 70,
  },
  {
    author: 'Алан Мур',
    name: 'Неономікон',
    price: 70,
  },
  {
    author: 'Террі Пратчетт',
    name: 'Рухомі картинки',
    price: 40,
  },
  {
    author: 'Анґус Гайленд',
    name: 'Коти в мистецтві',
  },
]

function createBookElement(book) {
  const listItem = document.createElement('li')
  listItem.innerHTML = `
      <p>Автор: ${book.author || 'Невідомий'}</p>
      <p>Назва: ${book.name || 'Без назви'}</p>
      <p>Ціна: ${book.price || 'Невідомо'}</p>
    `
  return listItem
}
function displayValidBooks(booksArray) {
  const rootElement = document.getElementById('root')
  const list = document.createElement('ul')

  booksArray.forEach((book) => {
    const requiredProps = ['author', 'name', 'price']
    try {
      requiredProps.forEach((prop) => {
        if (!book[prop]) {
          throw new Error(`Ця книга не містить властивості ${prop}`)
        }
      })

      const listItem = createBookElement(book)
      list.appendChild(listItem)
    } catch (error) {
      console.error(error)
    }
  })

  rootElement.appendChild(list)
}

displayValidBooks(books)
