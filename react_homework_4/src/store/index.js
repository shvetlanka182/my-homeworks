import { applyMiddleware, createStore } from 'redux'
import { composeWithDevTools } from '@redux-devtools/extension'
import thunk from 'redux-thunk'

const initialState = {
  products: [],
  favorite: [],
  cart: [],
  selectedProduct: null,
  isAcceptDeleteModal: false,
  isAcceptAddModal: false,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_PRODUCTS':
      return {
        ...state,
        products: [...action.payload],
      }
    case 'SET_CART':
      return {
        ...state,
        cart: action.payload,
      }
    case 'SET_FAVORITE':
      return {
        ...state,
        favorite: action.payload,
      }
    case 'ADD_TO_CART':
      return {
        ...state,
        cart: [...state.cart, action.payload],
      }
    case 'DELETE_FROM_CART':
      return {
        ...state,
        cart: state.cart.filter(
          (product) => product.article !== action.payload
        ),
      }
    case 'SET_PRODUCT':
      return {
        ...state,
        selectedProduct: action.payload,
      }
    case 'TOGGLE_ACCEPT_ADD_MODAL':
      return {
        ...state,
        isAcceptAddModal: !state.isAcceptAddModal,
      }
    case 'TOGGLE_ACCEPT_DELETE_MODAL':
      return {
        ...state,
        isAcceptDeleteModal: !state.isAcceptDeleteModal,
      }
    case 'CHANGE_QUANTITY':
      return {
        ...state,
        cart: state.cart.map((item) =>
          item.article === action.payload.article
            ? { ...item, quantity: action.payload.quantity }
            : item
        ),
      }
    default:
      return state
  }
}

export const setProducts = (data) => ({
  type: 'SET_PRODUCTS',
  payload: data,
})
export const setCart = (data) => ({
  type: 'SET_CART',
  payload: data,
})
export const setFavorite = (data) => ({
  type: 'SET_FAVORITE',
  payload: data,
})
export const addToCart = (selectedProduct) => ({
  type: 'ADD_TO_CART',
  payload: selectedProduct,
})
export const deleteFromCart = (id) => ({
  type: 'DELETE_FROM_CART',
  payload: id,
})
export const setSelectedProduct = (product) => ({
  type: 'SET_PRODUCT',
  payload: product,
})
export const toggleAcceptAddModal = () => ({
  type: 'TOGGLE_ACCEPT_ADD_MODAL',
})
export const toggleAcceptDeleteModal = () => ({
  type: 'TOGGLE_ACCEPT_DELETE_MODAL',
})
export const changeQuantity = (article, quantity) => ({
  type: 'CHANGE_QUANTITY',
  payload: { article, quantity },
})

export const fetchProducts = () => {
  return async (dispatch) => {
    const response = await fetch('/data.json')
    const products = await response.json()

    dispatch(setProducts(products))
  }
}

export const toggleProductInFavorite = (product) => {
  return (dispatch, getState) => {
    const currentFavorite = getState().favorite
    const index = currentFavorite.findIndex(
      (item) => item.article === product.article
    )
    if (index === -1) {
      const newFavorite = [...currentFavorite, product]
      dispatch(setFavorite(newFavorite))
    } else {
      const newFavorite = [...currentFavorite]
      newFavorite.splice(index, 1)
      dispatch(setFavorite(newFavorite))
    }
  }
}

export const handleAddToCart = (product) => {
  return (dispatch, setState) => {
    const updatedCart = setState().cart
    const itemIndex = updatedCart.findIndex(
      (item) => item.article === product.article
    )
    if (itemIndex !== -1) {
      updatedCart[itemIndex].quantity += 1
      dispatch(setCart(updatedCart))
    } else {
      dispatch(addToCart(product))
    }
  }
}

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))

export default store
