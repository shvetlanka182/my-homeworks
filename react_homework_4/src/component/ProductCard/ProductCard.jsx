import React from 'react'
import PropTypes from 'prop-types'
import ButtonOpenModal from '../ButtonOpenModal'
import styles from './ProductCard.module.scss'
import { AiFillStar, AiOutlineStar } from 'react-icons/ai'
import {
  setSelectedProduct,
  toggleAcceptAddModal,
  toggleProductInFavorite,
} from '../../store/index.js'
import { useDispatch, useSelector } from 'react-redux'

const ProductCard = ({ item }) => {
  const dispatch = useDispatch()
  const favorite = useSelector((state) => state.favorite)
  const handleSelectProduct = () => {
    dispatch(toggleAcceptAddModal())
    dispatch(setSelectedProduct(item))
  }

  return (
    <div className={styles.card}>
      <img src={item.image} alt={item.name} />
      <h5>{item.name}</h5>
      <div className={styles.price}>{item.price} грн.</div>
      <div
        className={styles.favorite}
        onClick={() => dispatch(toggleProductInFavorite(item))}
      >
        {favorite.some((product) => product.article === item.article) ? (
          <AiFillStar className={styles.svg_icon} />
        ) : (
          <AiOutlineStar className={styles.svg_icon} />
        )}
      </div>
      <ButtonOpenModal
        text="Add to cart"
        backgroundColor="orange"
        onClick={handleSelectProduct}
      />
    </div>
  )
}

ProductCard.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string,
    article: PropTypes.number.isRequired,
    color: PropTypes.string,
  }),
  toggleAcceptCartModal: PropTypes.func,
  toggleProductInCart: PropTypes.func,
}

export default ProductCard
