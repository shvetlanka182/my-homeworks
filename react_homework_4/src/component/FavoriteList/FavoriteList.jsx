import React from 'react'
import { useSelector } from 'react-redux'
import ProductCard from '../ProductCard/ProductCard'
import styles from './FavoriteList.module.scss'

const FavoriteList = () => {
  const favorite = useSelector((state) => state.favorite)
  return (
    <div className={styles.products}>
      {favorite.map((item) => {
        return <ProductCard item={item} key={item.article} />
      })}
    </div>
  )
}

export default FavoriteList
