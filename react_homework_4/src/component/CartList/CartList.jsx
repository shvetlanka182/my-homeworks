import React from 'react'
import { useSelector } from 'react-redux'
import CartItem from '../CartItem/CartItem'
import styles from './CartList.module.scss'

const CartList = () => {
  const cart = useSelector((state) => state.cart)
  return (
    <div className={styles.cart}>
      {!cart.length && 'Cart is empty'}
      {cart?.map((item, index) => {
        return <CartItem key={index} item={item} />
      })}
    </div>
  )
}

export default CartList
