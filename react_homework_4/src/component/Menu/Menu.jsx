import React from 'react'
import PropTypes from 'prop-types'
import styles from './Menu.module.scss'
import { Link } from 'react-router-dom'

const Menu = ({ cart, favorite }) => {
  return (
    <div className="container">
      <div className={styles.menu}>
        <Link to="/" className={styles.logo}></Link>
        {/* <div className={styles.link}>Shop</div>
        <div className={styles.link}>Men</div>
        <div className={styles.link}>Women</div>
        <div className={styles.link}>Combos</div>
        <div className={styles.link}>Joggers</div> */}
        <div className={styles.search}>
          <input
            type="text"
            placeholder="Search"
            className={styles.search_input}
          />
        </div>
        <div className={styles.icons}>
          <Link to="/favorite" className={styles.icon}>
            <div className={styles.count}>{favorite.length}</div>
            <div className={styles.favorite}></div>
          </Link>
          <Link to="/cart" className={styles.icon}>
            <div className={styles.count}>{cart.length}</div>
            <div className={styles.basket}></div>
          </Link>
        </div>
      </div>
    </div>
  )
}

Menu.propTypes = {
  cart: PropTypes.array,
  favorite: PropTypes.array,
}

export default Menu
