import React from 'react'
import ProductCard from '../ProductCard/ProductCard'
import styles from './ProductList.module.scss'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux'

const ProductList = () => {
  const products = useSelector((state) => state.products)
  return (
    <div className={styles.products}>
      {products.map((item) => {
        return <ProductCard item={item} key={item.article} />
      })}
    </div>
  )
}

ProductList.propTypes = {
  favorite: PropTypes.array,
  toggleAcceptCartModal: PropTypes.func,
  toggleProductInCart: PropTypes.func,
}

export default ProductList
