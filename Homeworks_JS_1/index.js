// 1.Змінну у Java Script можно оголосити за допомогою var, let, const.
// 2.Функція confirm повертає лише булевезначення, тобто true або false. Функція prompt - дає можливість користувачу вводити дані, повертає String. 
// 3.Неявне перетворення типів - це автоматична,примусова конвертація типів. Наприклад при використанні оператора нестрогої рівності відбувається неявне 
// перетворення типів для того, щоб можно було їх потім порівняти. 1 == '1'


// 1
const userName = "Svitlana";
const admin = userName;
console.log(admin);

// 2
const days = 2;
const seconds = days * 24 * 60 * 60;
console.log(seconds);

// 3
const userAge = prompt("How old are you?");
console.log(userAge)
