//1. DOM це об'єктна модель документа, програмний інтерфейс для HTML документів.
// DOM – це спосіб представлення веб-сторінок за допомогою набору об'єктів,
// вкладених один в одного. Таке дерево, потрібне для правильного відображення сайту
// та внесення змін на сторінках за допомогою JavaScript.

//2. innerText - показує будь-який текст, укладений між елементами, що відкривають
// і закривають тегами.
// innerHTML - покаже текстову інформацію по одному елементу.Тобто текст і розмітку
// HTML-документа, яка може бути укладена між тегами основного елемента, 
// що відкривають і закривають тегами.

//3. Для того, щоб звернутися до елемента сторінки за допомогою JS використовують:
// querySelector, querySelectorAll, getElementById, getElementsByClassName, getElementsByTagName,
// getElementsByName. Частіше використовують перші два способи.

//1.
const paragraph = document.querySelectorAll('p');
paragraph.forEach((elements) => {
elements.style.background = "#ff0000"
})
console.log(paragraph);

//2.
const optionsList = document.getElementById('optionsList');
console.log(optionsList);
const parentElement = optionsList.parentElement;
console.log(parentElement);
const childrenElement = optionsList.childNodes;

const arr = [];
for(let i = 0; i < childrenElement.length; i++) {
    arr.push([childrenElement[i].nodeName, childrenElement[i].nodeType]);
}
console.log(arr);

//3.
const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerText = 'This is a paragraph';
console.log(testParagraph);

//4.
const elementsMainHeader = document.querySelector('.main-header').children;
for(let i = 0; i < elementsMainHeader.length; i++) {
    elementsMainHeader[i].className = 'nav-item';
}
console.log(elementsMainHeader);

//5.
const sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach((elements) => {
    elements.classList.remove('section-title')
})
console.log(sectionTitle);