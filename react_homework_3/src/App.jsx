import React, { useEffect, useState } from 'react'
import './App.scss'
import ProductList from './component/ProductList/ProductList'
import Menu from './component/Menu/Menu'
import Modal from './component/Modal'
import { Route, Routes } from 'react-router-dom'
import CartList from './component/CartList/CartList'
import FavoriteList from './component/FavoriteList/FavoriteList'

function App() {
  const [selectedProduct, setSelectedProduct] = useState(null)
  const [cart, setCart] = useState(
    JSON.parse(localStorage.getItem('cart')) || []
  )
  const [favorite, setFavorite] = useState(
    JSON.parse(localStorage.getItem('favorite')) || []
  )
  const [isActiveAcceptCartModal, setIsActiveAcceptCartModal] = useState(false)
  const [isActiveAcceptDeleteModal, setIsActiveAcceptDeleteModal] =
    useState(false)

  const [data, setData] = useState([])

  useEffect(() => {
    fetch('/data.json')
      .then((response) => response.json())
      .then((data) => setData(data))
      .catch((err) => console.log('Щось пішло не по плану', err))
  }, [])

  const header = 'Do you want to delete this file?'
  const closeButton = true
  const text = `Once you delete this file, it won’t be possible to undo this action.
  Are you sure you want to delete it?`
  const actions = [
    <React.Fragment key="Ok">Ok</React.Fragment>,
    <React.Fragment key="Cancel">Cancel</React.Fragment>,
  ]

  const changeQuantity = (id, changeAmount = 1) => {
    const updatedCart = [...cart]
    const itemIndex = updatedCart.findIndex((item) => item.article === id)

    if (itemIndex !== -1) {
      updatedCart[itemIndex].quantity += changeAmount

      setCart(updatedCart)
    }
  }

  const addToCart = () => {
    const updatedCart = [...cart]
    const itemIndex = updatedCart.findIndex(
      (item) => item.article === selectedProduct.article
    )
    if (itemIndex !== -1) {
      updatedCart[itemIndex].quantity += 1
      setCart(updatedCart)
    } else {
      setCart([...cart, selectedProduct])
    }
  }

  const deleteFromCart = (id) => {
    const updatedCart = [...cart]
    setCart(updatedCart.filter((item) => item.article !== id))
  }

  const toggleProductInCart = (product) => {
    const currentFavorite = JSON.parse(localStorage.getItem('favorite')) || []
    const index = currentFavorite.findIndex(
      (item) => item.article === product.article
    )
    if (index === -1) {
      const newFavorite = [...currentFavorite, product]
      setFavorite(newFavorite)
    } else {
      const newFavorite = [...currentFavorite]
      newFavorite.splice(index, 1)
      setFavorite(newFavorite)
    }
  }
  useEffect(() => {
    localStorage.setItem('favorite', JSON.stringify(favorite))
  }, [favorite])

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart))
  }, [cart])

  const toggleAcceptCartModal = () =>
    setIsActiveAcceptCartModal(!isActiveAcceptCartModal)
  const toggleAcceptDeleteModal = () =>
    setIsActiveAcceptDeleteModal(!isActiveAcceptDeleteModal)

  return (
    <div className="App">
      <div className="container">
        <Menu cart={cart} favorite={favorite} />
      </div>

      <div className="container">
        <Routes>
          <Route
            path="/"
            element={
              <ProductList
                data={data}
                favorite={favorite}
                toggleAcceptCartModal={toggleAcceptCartModal}
                setSelectedProduct={setSelectedProduct}
                toggleProductInCart={toggleProductInCart}
              />
            }
          />
          <Route
            path="/favorite"
            element={
              <FavoriteList
                favorite={favorite}
                toggleAcceptCartModal={toggleAcceptCartModal}
                setSelectedProduct={setSelectedProduct}
                toggleProductInCart={toggleProductInCart}
              />
            }
          />
          <Route
            path="/cart"
            element={
              <CartList
                cart={cart}
                changeQuantity={changeQuantity}
                setSelectedProduct={setSelectedProduct}
                toggleAcceptDeleteModal={toggleAcceptDeleteModal}
              />
            }
          />
        </Routes>
      </div>

      {isActiveAcceptCartModal && (
        <Modal
          header={'Are sure you want to add the product to the cart?'}
          closeButton={true}
          actions={actions}
          closeModal={toggleAcceptCartModal}
          changeCart={addToCart}
        />
      )}
      {isActiveAcceptDeleteModal && (
        <Modal
          header={header}
          text={text}
          closeButton={closeButton}
          actions={actions}
          closeModal={toggleAcceptDeleteModal}
          changeCart={() => deleteFromCart(selectedProduct.article)}
        />
      )}
    </div>
  )
}

export default App
