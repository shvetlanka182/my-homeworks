import React from 'react'
import PropTypes from 'prop-types'
import ButtonOpenModal from '../ButtonOpenModal'
import styles from './ProductCard.module.scss'
import { AiFillStar, AiOutlineStar } from 'react-icons/ai'

const ProductCard = ({
  item,
  favorite,
  toggleAcceptCartModal,
  setSelectedProduct,
  toggleProductInCart,
}) => {
  const handleSelectProduct = () => {
    toggleAcceptCartModal()
    setSelectedProduct(item)
  }

  return (
    <div className={styles.card}>
      <img src={item.image} alt={item.name} />
      <h5>{item.name}</h5>
      <div className={styles.price}>{item.price} грн.</div>
      <div
        className={styles.favorite}
        onClick={() => toggleProductInCart(item)}
      >
        {favorite.some((product) => product.article === item.article) ? (
          <AiFillStar className={styles.svg_icon} />
        ) : (
          <AiOutlineStar className={styles.svg_icon} />
        )}
      </div>
      <ButtonOpenModal
        text="Add to cart"
        backgroundColor="orange"
        onClick={handleSelectProduct}
      />
    </div>
  )
}

ProductCard.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string,
    article: PropTypes.number.isRequired,
    color: PropTypes.string,
  }),
  favorite: PropTypes.array,
  toggleAcceptCartModal: PropTypes.func,
  setSelectedProduct: PropTypes.func,
  toggleProductInCart: PropTypes.func,
}

export default ProductCard
