import React from 'react'
import styles from './CartItem.module.scss'

const CartItem = ({
  item,
  changeQuantity,
  setSelectedProduct,
  toggleAcceptDeleteModal,
}) => {
  const handleDeleteFromCart = () => {
    toggleAcceptDeleteModal()
    setSelectedProduct(item)
  }

  return (
    <div className={styles.item}>
      <div className={styles.item_image}>
        <img src={item.image} alt={item.name} />
      </div>
      <div className={styles.item_name}>{item.name}</div>
      <div className={styles.item_count}>
        <button
          className={styles.item_count_btn}
          onClick={() => changeQuantity(item.article, 1)}
        >
          +
        </button>
        <span>{item.quantity}</span>
        <button
          className={styles.item_count_btn}
          onClick={() => item.quantity > 1 && changeQuantity(item.article, -1)}
        >
          -
        </button>
      </div>
      <div className={styles.item_price}>{item.price}</div>
      <button className={styles.item_delete} onClick={handleDeleteFromCart}>
        x
      </button>
    </div>
  )
}
export default CartItem
