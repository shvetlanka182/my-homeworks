import React from 'react'
import CartItem from '../CartItem/CartItem'
import styles from './CartList.module.scss'

const CartList = ({
  cart,
  changeQuantity,
  setSelectedProduct,
  toggleAcceptDeleteModal,
}) => {
  return (
    <div className={styles.cart}>
      {!cart.length && 'Cart is empty'}
      {cart?.map((item, index) => {
        return (
          <CartItem
            key={index}
            item={item}
            changeQuantity={changeQuantity}
            setSelectedProduct={setSelectedProduct}
            toggleAcceptDeleteModal={toggleAcceptDeleteModal}
          />
        )
      })}
    </div>
  )
}

export default CartList
