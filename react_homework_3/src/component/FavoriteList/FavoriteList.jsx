import React from 'react'
import ProductCard from '../ProductCard/ProductCard'
import styles from './FavoriteList.module.scss'

const FavoriteList = ({
  favorite,
  toggleAcceptCartModal,
  setSelectedProduct,
  toggleProductInCart,
}) => {
  return (
    <div className={styles.products}>
      {favorite.map((item) => {
        return (
          <ProductCard
            item={item}
            favorite={favorite}
            key={item.article}
            toggleAcceptCartModal={toggleAcceptCartModal}
            setSelectedProduct={setSelectedProduct}
            toggleProductInCart={toggleProductInCart}
          />
        )
      })}
    </div>
  )
}

export default FavoriteList
