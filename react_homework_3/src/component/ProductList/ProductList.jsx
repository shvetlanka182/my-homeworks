import React from 'react'
import ProductCard from '../ProductCard/ProductCard'
import styles from './ProductList.module.scss'
import PropTypes from 'prop-types'

const ProductList = ({
  data,
  favorite,
  toggleAcceptCartModal,
  setSelectedProduct,
  toggleProductInCart,
}) => {
  return (
    <div className={styles.products}>
      {data.map((item) => {
        return (
          <ProductCard
            item={item}
            favorite={favorite}
            key={item.article}
            toggleAcceptCartModal={toggleAcceptCartModal}
            setSelectedProduct={setSelectedProduct}
            toggleProductInCart={toggleProductInCart}
          />
        )
      })}
    </div>
  )
}

ProductList.propTypes = {
  data: PropTypes.array.isRequired,
  favorite: PropTypes.array,
  toggleAcceptCartModal: PropTypes.func,
  setSelectedProduct: PropTypes.func,
  toggleProductInCart: PropTypes.func,
}

export default ProductList
