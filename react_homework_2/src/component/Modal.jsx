import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Shadow = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;
`

const ModalWindow = styled.div`
  width: 516px;
  background-color: #e74c3c;
  position: relative;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
  padding-bottom: 25px;
`

const Header = styled.div`
  background-color: #d44637;
  color: #fff;
  height: 68px;
  padding: 0 30px;
  font-size: 18px;
  font-weight: 500;
  line-height: 68px;
  text-align: left;
  margin-bottom: 15px;
`

const Text = styled.div`
  padding: 40px 40px 35px 40px;
  font-size: 15px;
  font-weight: 400;
  line-height: 30px;
  color: #fff;
`
const Button = styled.div`
  min-width: 101px;
  padding: 0 5px;
  height: 41px;
  background-color: #b3382c;
  font-size: 15px;
  line-height: 41px;
  text-align: center;
  color: #ffffff;
  border-radius: 5px;
  cursor: pointer;
  margin: 0 5px;

  &:hover {
    opacity: 0.7;
  }
`

const CloseButton = styled.div`
  width: 20px;
  height: 20px;
  position: absolute;
  right: 30px;
  top: 25px;
  background-image: url('/cross.png');
  cursor: pointer;
  transition: 1s;
  display: ${(props) => (props.$closeButton ? 'block' : 'none')};

  &:hover {
    transform: rotate(180deg);
  }
`

const Modal = ({
  header,
  closeButton,
  text,
  actions,
  closeModal,
  addToCart,
}) => {
  const handlerCloseModal = (e) => {
    if (e.target.classList.contains('hide')) {
      closeModal()
    }
  }

  const handlerAcceptAddToCart = () => {
    addToCart()
    closeModal()
  }

  return (
    <Shadow onClick={handlerCloseModal} className="hide">
      <ModalWindow>
        <CloseButton
          $closeButton={closeButton}
          onClick={closeModal}
        ></CloseButton>
        <Header>{header}</Header>
        {text && <Text>{text}</Text>}
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          {actions.map((button) => {
            return (
              <Button
                key={button.key}
                onClick={() =>
                  button.key === 'Cancel'
                    ? closeModal()
                    : handlerAcceptAddToCart()
                }
              >
                {button}
              </Button>
            )
          })}
        </div>
      </ModalWindow>
    </Shadow>
  )
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.array,
  closeModal: PropTypes.func,
  addToCart: PropTypes.func,
}

export default Modal
