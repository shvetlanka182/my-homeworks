import React, { useEffect, useState } from 'react'
import './App.scss'
import ButtonOpenModal from './component/ButtonOpenModal'
import ProductList from './component/ProductList/ProductList'
import Menu from './component/Menu/Menu'
import Modal from './component/Modal'

function App() {
  const [isActiveFirstModal, setIsActiveFirstModal] = useState(false)
  const [selectedProduct, setSelectedProduct] = useState(null)
  const [cart, setCart] = useState(
    JSON.parse(localStorage.getItem('cart')) || []
  )
  const [favorite, setFavorite] = useState(
    JSON.parse(localStorage.getItem('favorite')) || []
  )
  const [isActiveAcceptCartModal, setIsActiveAcceptCartModal] = useState(false)
  const [data, setData] = useState([])

  useEffect(() => {
    fetch('/data.json')
      .then((response) => response.json())
      .then((data) => setData(data))
      .catch((err) => console.log('Щось пішло не по плану', err))
  }, [])

  const header = 'Do you want to delete this file?'
  const closeButton = true
  const text = `Once you delete this file, it won’t be possible to undo this action.
  Are you sure you want to delete it?`
  const actions = [
    <React.Fragment key="Ok">Ok</React.Fragment>,
    <React.Fragment key="Cancel">Cancel</React.Fragment>,
  ]

  const addToCart = () => {
    setCart([...cart, selectedProduct])
  }

  const toggleProductInCart = (product) => {
    const currentFavorite = JSON.parse(localStorage.getItem('favorite')) || []
    const index = currentFavorite.findIndex(
      (item) => item.article === product.article
    )
    if (index === -1) {
      const newFavorite = [...currentFavorite, product]
      setFavorite(newFavorite)
    } else {
      const newFavorite = [...currentFavorite]
      newFavorite.splice(index, 1)
      setFavorite(newFavorite)
    }
  }
  useEffect(() => {
    localStorage.setItem('favorite', JSON.stringify(favorite))
  }, [favorite])

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart))
  }, [cart])

  const toggleFirstModal = () => setIsActiveFirstModal(!isActiveFirstModal)
  const toggleAcceptCartModal = () =>
    setIsActiveAcceptCartModal(!isActiveAcceptCartModal)

  return (
    <div className="App">
      <div className="container">
        <Menu cart={cart} favorite={favorite} />
      </div>

      {/* <img
        src="/girlwithbags.jpg"
        alt="Дівчина з торбами"
        className="girlwithbags"
      /> */}

      <div className="container">
        <ProductList
          data={data}
          favorite={favorite}
          toggleAcceptCartModal={toggleAcceptCartModal}
          setSelectedProduct={setSelectedProduct}
          toggleProductInCart={toggleProductInCart}
        />
      </div>

      {isActiveAcceptCartModal && (
        <Modal
          header={'Are sure you want to add the product to the cart?'}
          closeButton={true}
          actions={actions}
          closeModal={toggleAcceptCartModal}
          addToCart={addToCart}
        />
      )}

      {/* {isActiveFirstModal && (
        <Modal
          header={header}
          closeButton={closeButton}
          text={text}
          actions={actions}
          closeModal={toggleFirstModal}
        />
      )} */}
      {/* <ButtonOpenModal
        text="Open First Modal"
        backgroundColor="blue"
        onClick={toggleFirstModal}
      /> */}
    </div>
  )
}

export default App
