const dotaImages = document.querySelectorAll('.image-to-show');
let currentImage = 1;
let prevImage = 0;
let imagesInterval = false;
function startInterval(){
    if(!imagesInterval){
        imagesInterval = setInterval(showNextImage,3000);
    }
    
};
function stopInterval(){
    clearInterval(imagesInterval);
    imagesInterval = false;
};
 
function showNextImage() {
    dotaImages[prevImage].classList.remove('show');
    dotaImages[currentImage].classList.add('show');
    prevImage = (prevImage+1)%dotaImages.length;
    currentImage = (currentImage+1)%dotaImages.length;
};

const stopBtn = document.getElementById('stop-btn');
const startBtn = document.getElementById('start-btn');
stopBtn.addEventListener('click', () => {
    console.log(imagesInterval);
    stopInterval(); 
});

startBtn.addEventListener('click', () => {
    console.log(imagesInterval);
    startInterval();
});

startInterval();
    



